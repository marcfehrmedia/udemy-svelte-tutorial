import { derived } from 'svelte/store';
import settings from './settings';
import location from './location';

const direction = derived(
	[location, settings],
	([$location, $settings], set) => {
		console.log($location, $settings);
		const timer = setTimeout(() => {
			set($settings.language === 'ar' ? 'rtl' : 'ltr');
		}, 2000);
		return () => {
			clearTimeout(timer);
		};
	},
	'loading'
);

export default direction;
